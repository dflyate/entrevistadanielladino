SELECT vh.placa, em.tipo_documento, em.numero_documento, 
em.nombre_completo, 
(select COUNT(c.id_conductor) FROM Conductor c INNER JOIN conductor_vehiculo cv on c.id_conductor = cv.idConductor) as Conductores_vinculados
FROM Vehiculo vh inner join Empresa em on em.id_empresa = vh.id_empresa
WHERE (select COUNT(c.id_conductor) FROM Conductor c INNER JOIN conductor_vehiculo cv on c.id_conductor = cv.idConductor) > 2
ORDER BY vh.placa asc