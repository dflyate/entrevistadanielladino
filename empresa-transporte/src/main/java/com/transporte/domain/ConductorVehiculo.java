/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "conductor_vehiculo")
@NamedQueries({
    @NamedQuery(name = "ConductorVehiculo.findAll", query = "SELECT c FROM ConductorVehiculo c"),
    @NamedQuery(name = "ConductorVehiculo.findByIdConductorVehiculo", query = "SELECT c FROM ConductorVehiculo c WHERE c.idConductorVehiculo = :idConductorVehiculo"),
    @NamedQuery(name = "ConductorVehiculo.delete", query = "DELETE FROM ConductorVehiculo e WHERE e.idConductorVehiculo = :id_conductor_vehiculo")})
public class ConductorVehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_conductor_vehiculo")
    private Integer idConductorVehiculo;
    @Column(name = "idConductor")
    private Integer idConductor;
    @Column(name = "idVehiculo")
    private Integer idVehiculo;

    public ConductorVehiculo() {
    }

    public ConductorVehiculo(Integer idConductorVehiculo) {
        this.idConductorVehiculo = idConductorVehiculo;
    }

    public ConductorVehiculo(Integer idConductorVehiculo, int idConductor, int idVehiculo) {
        this.idConductorVehiculo = idConductorVehiculo;
        this.idConductor = idConductor;
        this.idVehiculo = idVehiculo;
    }

    public Integer getIdConductorVehiculo() {
        return idConductorVehiculo;
    }

    public void setIdConductorVehiculo(Integer idConductorVehiculo) {
        this.idConductorVehiculo = idConductorVehiculo;
    }

    public Integer getIdConductor() {
        return idConductor;
    }

    public void setIdConductor(Integer idConductor) {
        this.idConductor = idConductor;
    }

    public Integer getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConductorVehiculo != null ? idConductorVehiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConductorVehiculo)) {
            return false;
        }
        ConductorVehiculo other = (ConductorVehiculo) object;
        if ((this.idConductorVehiculo == null && other.idConductorVehiculo != null) || (this.idConductorVehiculo != null && !this.idConductorVehiculo.equals(other.idConductorVehiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gm.sga.datos.ConductorVehiculo[ idConductorVehiculo=" + idConductorVehiculo + " ]";
    }
    
}
