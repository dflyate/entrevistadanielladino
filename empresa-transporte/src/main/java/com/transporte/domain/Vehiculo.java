/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Vehiculo.findAll", query = "SELECT v FROM Vehiculo v"),
    @NamedQuery(name = "Vehiculo.findByIdVehiculo", query = "SELECT v FROM Vehiculo v WHERE v.idVehiculo = :idVehiculo"),
    @NamedQuery(name = "Vehiculo.findByPlaca", query = "SELECT v FROM Vehiculo v WHERE v.placa = :placa"),
    @NamedQuery(name = "Vehiculo.findByMotor", query = "SELECT v FROM Vehiculo v WHERE v.motor = :motor"),
    @NamedQuery(name = "Vehiculo.findByChasis", query = "SELECT v FROM Vehiculo v WHERE v.chasis = :chasis"),
    @NamedQuery(name = "Vehiculo.findByModelo", query = "SELECT v FROM Vehiculo v WHERE v.modelo = :modelo"),
    @NamedQuery(name = "Vehiculo.findByFechaMatricula", query = "SELECT v FROM Vehiculo v WHERE v.fechaMatricula = :fechaMatricula"),
    @NamedQuery(name = "Vehiculo.findByPasajerosSentados", query = "SELECT v FROM Vehiculo v WHERE v.pasajerosSentados = :pasajerosSentados"),
    @NamedQuery(name = "Vehiculo.findByPasajerosPie", query = "SELECT v FROM Vehiculo v WHERE v.pasajerosPie = :pasajerosPie"),
    @NamedQuery(name = "Vehiculo.findByPesoSeco", query = "SELECT v FROM Vehiculo v WHERE v.pesoSeco = :pesoSeco"),
    @NamedQuery(name = "Vehiculo.findByPesoBruto", query = "SELECT v FROM Vehiculo v WHERE v.pesoBruto = :pesoBruto"),
    @NamedQuery(name = "Vehiculo.findByPuertas", query = "SELECT v FROM Vehiculo v WHERE v.puertas = :puertas"),
    @NamedQuery(name = "Vehiculo.findByMarca", query = "SELECT v FROM Vehiculo v WHERE v.marca = :marca"),
    @NamedQuery(name = "Vehiculo.findByLinea", query = "SELECT v FROM Vehiculo v WHERE v.linea = :linea"),
    @NamedQuery(name = "Vehiculo.delete", query = "DELETE FROM Vehiculo e WHERE e.idVehiculo = :idVehiculo")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Vehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_vehiculo")
    private Integer idVehiculo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String placa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String motor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String chasis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String modelo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "fecha_matricula")
    private String fechaMatricula;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pasajeros_sentados")
    private int pasajerosSentados;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pasajeros_pie")
    private int pasajerosPie;
    @Size(max = 45)
    @Column(name = "peso_seco")
    private String pesoSeco;
    @Size(max = 45)
    @Column(name = "peso_bruto")
    private String pesoBruto;
    @Basic(optional = false)
    @NotNull
    private int puertas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String marca;
    @Size(max = 45)
    private String linea;
    @Column(name = "id_empresa")
    private Integer idEmpresa;

    public Vehiculo() {
    }

    public Vehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public Vehiculo(Integer idVehiculo, String placa, String motor, String chasis, String modelo, String fechaMatricula, int pasajerosSentados, int pasajerosPie, int puertas, String marca) {
        this.idVehiculo = idVehiculo;
        this.placa = placa;
        this.motor = motor;
        this.chasis = chasis;
        this.modelo = modelo;
        this.fechaMatricula = fechaMatricula;
        this.pasajerosSentados = pasajerosSentados;
        this.pasajerosPie = pasajerosPie;
        this.puertas = puertas;
        this.marca = marca;
    }

    public Integer getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFechaMatricula() {
        return fechaMatricula;
    }

    public void setFechaMatricula(String fechaMatricula) {
        this.fechaMatricula = fechaMatricula;
    }

    public int getPasajerosSentados() {
        return pasajerosSentados;
    }

    public void setPasajerosSentados(int pasajerosSentados) {
        this.pasajerosSentados = pasajerosSentados;
    }

    public int getPasajerosPie() {
        return pasajerosPie;
    }

    public void setPasajerosPie(int pasajerosPie) {
        this.pasajerosPie = pasajerosPie;
    }

    public String getPesoSeco() {
        return pesoSeco;
    }

    public void setPesoSeco(String pesoSeco) {
        this.pesoSeco = pesoSeco;
    }

    public String getPesoBruto() {
        return pesoBruto;
    }

    public void setPesoBruto(String pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVehiculo != null ? idVehiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehiculo)) {
            return false;
        }
        Vehiculo other = (Vehiculo) object;
        if ((this.idVehiculo == null && other.idVehiculo != null) || (this.idVehiculo != null && !this.idVehiculo.equals(other.idVehiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gm.sga.datos.Vehiculo[ idVehiculo=" + idVehiculo + " ]";
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    
}
