package com.transporte.servicio;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.transporte.datos.ConductorVehiculoDao;
import com.transporte.domain.ConductorVehiculo;
@Stateless
public class ConductoVehiculoServiceImpl implements ConductorVehiculoService{

    @Inject
    private ConductorVehiculoDao conductorDao;

    @Resource
    private SessionContext contexto;

    @Override
    public List<ConductorVehiculo> listarConductorsVehiculos() {
        return conductorDao.findAllConductorsVehiculos();
    }

    @Override
    public ConductorVehiculo encontrarConductorVehiculoPorId(ConductorVehiculo conductor) {
        return conductorDao.findConductorVehiculoById(conductor);
    }

    @Override
    public void registrarConductorVehiculo(ConductorVehiculo conductor) {
        conductorDao.insertConductorVehiculo(conductor);
    }

    @Override
    public void modificarConductorVehiculo(ConductorVehiculo conductor) {
        try {
            conductorDao.updateConductorVehiculo(conductor);
        } catch (Throwable t) {
            contexto.setRollbackOnly();
            t.printStackTrace(System.out);
        }
    }

    @Override
    public void eliminarConductorVehiculo(ConductorVehiculo conductor) {
        conductorDao.deleteConductorVehiculo(conductor);
    }

    
}
