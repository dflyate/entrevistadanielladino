package com.transporte.servicio;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.transporte.domain.Empresa;

@Path("/empresas")
@Stateless
public class EmpresaServiceRS {

    @Inject
    private EmpresaService empresaService;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Empresa> listarEmpresas() {
        return empresaService.listarEmpresas();
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") //empresas/{id}
    public Empresa encontrarEmpresaPorId(@PathParam("id") int id) {
        return empresaService.encontrarEmpresaPorId(new Empresa(id));
    }

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response agregarEmpresa(Empresa empresa) {
        try {
            empresaService.registrarEmpresa(empresa);
            return Response.ok().entity(empresa).build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") 
    public Response modificarEmpresa(@PathParam("id") int id, Empresa empresaModificada) {
        try {
            Empresa empresa = empresaService.encontrarEmpresaPorId(new Empresa(id));
            if(empresa != null){
                empresaService.modificarEmpresa(empresaModificada);
                return Response.ok().entity(empresaModificada).build();
            }else{
                return Response.status(Status.NOT_FOUND).build();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @DELETE
    @Path("{id}") 
    public Response eliminarEmpresaPorId(@PathParam("id") int id) {
        try {
            empresaService.eliminarEmpresa(new Empresa(id));
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }
    }
}
