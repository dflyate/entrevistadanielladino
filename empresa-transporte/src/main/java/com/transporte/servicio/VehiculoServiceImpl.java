package com.transporte.servicio;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.transporte.datos.VehiculoDao;
import com.transporte.domain.Vehiculo;

@Stateless
public class VehiculoServiceImpl implements VehiculoService{

    @Inject
    private VehiculoDao vehiculoDao;

    @Resource
    private SessionContext contexto;

    @Override
    public List<Vehiculo> listarVehiculos() {
        return vehiculoDao.findAllVehiculos();
    }

    @Override
    public Vehiculo encontrarVehiculoPorId(Vehiculo vehiculo) {
        return vehiculoDao.findVehiculoById(vehiculo);
    }

    @Override
    public void registrarVehiculo(Vehiculo vehiculo) {
        vehiculoDao.insertVehiculo(vehiculo);
    }

    @Override
    public void modificarVehiculo(Vehiculo vehiculo) {
        try {
            vehiculoDao.updateVehiculo(vehiculo);
        } catch (Throwable t) {
            contexto.setRollbackOnly();
            t.printStackTrace(System.out);
        }
    }

    @Override
    public void eliminarVehiculo(Vehiculo vehiculo) {
        vehiculoDao.deleteVehiculo(vehiculo);
    }

}
