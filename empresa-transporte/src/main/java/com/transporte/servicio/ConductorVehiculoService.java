/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.servicio;

import java.util.List;
import javax.ejb.Local;
import com.transporte.domain.ConductorVehiculo;

/**
 *
 * @author Usuario
 */
@Local
public interface ConductorVehiculoService {
    public List<ConductorVehiculo> listarConductorsVehiculos();
    public ConductorVehiculo encontrarConductorVehiculoPorId(ConductorVehiculo conductor);
    public void registrarConductorVehiculo(ConductorVehiculo conductor);
    public void modificarConductorVehiculo(ConductorVehiculo conductor);
    public void eliminarConductorVehiculo(ConductorVehiculo conductor);
}
