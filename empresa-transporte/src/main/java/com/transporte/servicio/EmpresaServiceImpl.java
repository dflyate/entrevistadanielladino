package com.transporte.servicio;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.transporte.datos.EmpresaDao;
import com.transporte.domain.Empresa;

@Stateless
public class EmpresaServiceImpl implements EmpresaService{

    @Inject
    private EmpresaDao empresaDao;

    @Resource
    private SessionContext contexto;

    @Override
    public List<Empresa> listarEmpresas() {
        return empresaDao.findAllEmpresas();
    }

    @Override
    public Empresa encontrarEmpresaPorId(Empresa empresa) {
        return empresaDao.findEmpresaById(empresa);
    }

    @Override
    public void registrarEmpresa(Empresa empresa) {
        empresaDao.insertEmpresa(empresa);
    }

    @Override
    public void modificarEmpresa(Empresa empresa) {
        try {
            empresaDao.updateEmpresa(empresa);
        } catch (Throwable t) {
            contexto.setRollbackOnly();
            t.printStackTrace(System.out);
        }
    }

    @Override
    public void eliminarEmpresa(Empresa empresa) {
        empresaDao.deleteEmpresa(empresa);
    }

}
