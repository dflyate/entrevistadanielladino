/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.servicio;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.transporte.domain.Conductor;

/**
 *
 * @author Usuario
 */
@Path("/conductores")
@Stateless
public class ConductorServiceRS {
    @Inject
    private ConductorService conductorService;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Conductor> listarConductors() {
        return conductorService.listarConductors();
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") //conductors/{id}
    public Conductor encontrarConductorPorId(@PathParam("id") int id) {
        return conductorService.encontrarConductorPorId(new Conductor(id));
    }

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response agregarConductor(Conductor conductor) {
        try {
            conductorService.registrarConductor(conductor);
            return Response.ok().entity(conductor).build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") 
    public Response modificarConductor(@PathParam("id") int id, Conductor conductorModificada) {
        try {
            Conductor conductor = conductorService.encontrarConductorPorId(new Conductor(id));
            if(conductor != null){
                conductorService.modificarConductor(conductorModificada);
                return Response.ok().entity(conductorModificada).build();
            }else{
                return Response.status(Status.NOT_FOUND).build();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @DELETE
    @Path("{id}") 
    public Response eliminarConductorPorId(@PathParam("id") int id) {
        try {
            conductorService.eliminarConductor(new Conductor(id));
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }
    }
}
