/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.servicio;

import java.util.List;
import javax.ejb.Local;
import com.transporte.domain.Vehiculo;

/**
 *
 * @author Usuario
 */
@Local
public interface VehiculoService {
    public List<Vehiculo> listarVehiculos();
    public Vehiculo encontrarVehiculoPorId(Vehiculo vehiculo);
    public void registrarVehiculo(Vehiculo vehiculo);
    public void modificarVehiculo(Vehiculo vehiculo);
    public void eliminarVehiculo(Vehiculo vehiculo);
}
