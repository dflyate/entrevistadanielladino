package com.transporte.servicio;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.transporte.domain.Vehiculo;

@Path("/vehiculos")
@Stateless
public class VehiculoServiceRS {

    @Inject
    private VehiculoService vehiculoService;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Vehiculo> listarVehiculos() {
        return vehiculoService.listarVehiculos();
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") //vehiculos/{id}
    public Vehiculo encontrarVehiculoPorId(@PathParam("id") int id) {
        return vehiculoService.encontrarVehiculoPorId(new Vehiculo(id));
    }

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response agregarVehiculo(Vehiculo vehiculo) {
        try {
            vehiculoService.registrarVehiculo(vehiculo);
            return Response.ok().entity(vehiculo).build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") 
    public Response modificarVehiculo(@PathParam("id") int id, Vehiculo vehiculoModificada) {
        try {
            Vehiculo vehiculo = vehiculoService.encontrarVehiculoPorId(new Vehiculo(id));
            if(vehiculo != null){
                vehiculoService.modificarVehiculo(vehiculoModificada);
                return Response.ok().entity(vehiculoModificada).build();
            }else{
                return Response.status(Status.NOT_FOUND).build();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @DELETE
    @Path("{id}") 
    public Response eliminarVehiculoPorId(@PathParam("id") int id) {
        try {
            vehiculoService.eliminarVehiculo(new Vehiculo(id));
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }
    }
}
