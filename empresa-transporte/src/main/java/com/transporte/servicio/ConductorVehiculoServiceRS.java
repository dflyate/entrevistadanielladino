/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.servicio;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.transporte.domain.Conductor;
import com.transporte.domain.ConductorVehiculo;

/**
 *
 * @author Usuario
 */
@Path("/conductores_vehiculos")
@Stateless
public class ConductorVehiculoServiceRS {
    @Inject
    private ConductorVehiculoService conductorService;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<ConductorVehiculo> listarConductors() {
        return conductorService.listarConductorsVehiculos();
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") //conductors/{id}
    public ConductorVehiculo encontrarConductorPorId(@PathParam("id") int id) {
        return conductorService.encontrarConductorVehiculoPorId(new ConductorVehiculo(id));
    }

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response agregarConductor(ConductorVehiculo conductor) {
        try {
            conductorService.registrarConductorVehiculo(conductor);
            return Response.ok().entity(conductor).build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("{id}") 
    public Response modificarConductor(@PathParam("id") int id, ConductorVehiculo conductorModificada) {
        try {
            ConductorVehiculo conductor = conductorService.encontrarConductorVehiculoPorId(new ConductorVehiculo(id));
            if(conductor != null){
                conductorService.modificarConductorVehiculo(conductorModificada);
                return Response.ok().entity(conductorModificada).build();
            }else{
                return Response.status(Status.NOT_FOUND).build();
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @DELETE
    @Path("{id}") 
    public Response eliminarConductorPorId(@PathParam("id") int id) {
        try {
            conductorService.eliminarConductorVehiculo(new ConductorVehiculo(id));
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return Response.status(404).build();
        }
    }
}
