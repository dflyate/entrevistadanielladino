package com.transporte.servicio;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.transporte.datos.ConductorDao;
import com.transporte.domain.Conductor;

@Stateless
public class ConductorServiceImpl implements ConductorService{

    @Inject
    private ConductorDao conductorDao;

    @Resource
    private SessionContext contexto;

    @Override
    public List<Conductor> listarConductors() {
        return conductorDao.findAllConductors();
    }

    @Override
    public Conductor encontrarConductorPorId(Conductor conductor) {
        return conductorDao.findConductorById(conductor);
    }

    @Override
    public void registrarConductor(Conductor conductor) {
        conductorDao.insertConductor(conductor);
    }

    @Override
    public void modificarConductor(Conductor conductor) {
        try {
            conductorDao.updateConductor(conductor);
        } catch (Throwable t) {
            contexto.setRollbackOnly();
            t.printStackTrace(System.out);
        }
    }

    @Override
    public void eliminarConductor(Conductor conductor) {
        conductorDao.deleteConductor(conductor);
    }

}
