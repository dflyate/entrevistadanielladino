/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.datos;

import java.util.List;
import com.transporte.domain.Empresa;

/**
 *
 * @author Usuario
 */
public interface EmpresaDao {
    public List<Empresa> findAllEmpresas();
    
    public Empresa findEmpresaById(Empresa empresa);
    
    public void insertEmpresa(Empresa empresa);
    
    public void updateEmpresa(Empresa empresa);
    
    public void deleteEmpresa(Empresa empresa);
}
