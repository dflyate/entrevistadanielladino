/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.datos;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.transporte.domain.Conductor;

/**
 *
 * @author Usuario
 */
@Stateless
public class ConductorDaoImpl implements ConductorDao{
    
    @PersistenceContext(unitName="SgaPU")
    EntityManager em;

    @Override
    public List<Conductor> findAllConductors() {
        return em.createNamedQuery("Conductor.findAll").getResultList();
    }

    @Override
    public void insertConductor(Conductor conductor) {
        em.persist(conductor);
    }

    @Override
    public void updateConductor(Conductor conductor) {
        em.merge(conductor);
    }

    @Override
    public void deleteConductor(Conductor conductor) {
        em.createNamedQuery("Conductor.delete").setParameter("idConductor", conductor.getIdConductor()).executeUpdate();
    }

    @Override
    public Conductor findConductorById(Conductor conductor) {
        return em.find(Conductor.class, conductor.getIdConductor());
    }
    
}
