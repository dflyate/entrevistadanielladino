/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.datos;

import java.util.List;
import com.transporte.domain.ConductorVehiculo;

/**
 *
 * @author Usuario
 */
public interface ConductorVehiculoDao {
    public List<ConductorVehiculo> findAllConductorsVehiculos();
    
    public ConductorVehiculo findConductorVehiculoById(ConductorVehiculo conductor);
    
    public void insertConductorVehiculo(ConductorVehiculo conductor);
    
    public void updateConductorVehiculo(ConductorVehiculo conductor);
    
    public void deleteConductorVehiculo(ConductorVehiculo conductor);
}
