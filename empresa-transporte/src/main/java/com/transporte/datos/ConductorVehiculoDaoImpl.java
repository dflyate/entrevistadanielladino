/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.datos;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.transporte.domain.Conductor;
import com.transporte.domain.ConductorVehiculo;

/**
 *
 * @author Usuario
 */
@Stateless
public class ConductorVehiculoDaoImpl implements ConductorVehiculoDao{
    
    @PersistenceContext(unitName="SgaPU")
    EntityManager em;

    @Override
    public List<ConductorVehiculo> findAllConductorsVehiculos() {
        return em.createNamedQuery("ConductorVehiculo.findAll").getResultList();
    }

    @Override
    public ConductorVehiculo findConductorVehiculoById(ConductorVehiculo conductor) {
        return em.find(ConductorVehiculo.class, conductor.getIdConductorVehiculo());
    }

    @Override
    public void insertConductorVehiculo(ConductorVehiculo conductor) {
        em.persist(conductor);
    }

    @Override
    public void updateConductorVehiculo(ConductorVehiculo conductor) {
        em.merge(conductor);
    }

    @Override
    public void deleteConductorVehiculo(ConductorVehiculo conductor) {
        em.createNamedQuery("ConductorVehiculo.delete").setParameter("id_conductor_vehiculo", conductor.getIdConductorVehiculo()).executeUpdate();
    }
    
    
}
