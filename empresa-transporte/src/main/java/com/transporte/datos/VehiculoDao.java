/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.datos;

import java.util.List;
import com.transporte.domain.Vehiculo;

/**
 *
 * @author Usuario
 */
public interface VehiculoDao {
    public List<Vehiculo> findAllVehiculos();
    
    public Vehiculo findVehiculoById(Vehiculo vehiculo);
    
    public void insertVehiculo(Vehiculo vehiculo);
    
    public void updateVehiculo(Vehiculo vehiculo);
   
    
    public void deleteVehiculo(Vehiculo vehiculo);
}
