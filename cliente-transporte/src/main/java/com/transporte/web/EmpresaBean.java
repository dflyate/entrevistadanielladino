
package com.transporte.web;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.client.Entity;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.transporte.domain.Empresa;
import org.glassfish.jersey.client.ClientConfig;
import org.primefaces.event.RowEditEvent;

@Named("empresaBean")
@RequestScoped
public class EmpresaBean {
    
    ClientConfig clientConfig = new ClientConfig();
    Client cliente = ClientBuilder.newClient(clientConfig);
    private static Invocation.Builder invocationBuilder;
    private static Response response;
    private static WebTarget webTarget;
    public static String ROL = "";
    
    private Empresa empresaSeleccionada;
    
    List<Empresa> empresas;
    
    public EmpresaBean(){
    }
    
    @PostConstruct
    public void inicializar(){
        //inicializar variables
        empresas = new ArrayList<>();
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/empresas");
        empresas = webTarget.request(MediaType.APPLICATION_XML).get(Response.class).readEntity(new GenericType<List<Empresa>>(){});
        empresaSeleccionada = new Empresa();
    }
    
    public String navegar(String rol){
        ROL = rol;
        if(rol.equals("ADMIN"))
            return "listarEmpresas";
        return "seleccionarOpcionUsuario";
    }
    
    public String navegarSiguiente(String rol){
        ROL = rol;
        return "listarEmpresas";
    }
    
    public String obtenerRol(){
        return ROL;
    }
    
    public void editListener(RowEditEvent event){
        Empresa empresa = (Empresa) event.getObject();
        String path= "/" + empresa.getIdEmpresa();
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/empresas");
        invocationBuilder = webTarget.path(path).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.put(Entity.entity(empresa,MediaType.APPLICATION_XML));
    }

    public Empresa getEmpresaSeleccionada() {
        return empresaSeleccionada;
    }

    public void setEmpresaSeleccionada(Empresa empresaSeleccionada) {
        this.empresaSeleccionada = empresaSeleccionada;
    }

    public List<Empresa> getEmpresas() {
        
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }
    
    public void agregarEmpresa(){
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/empresas");
        invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
        response=invocationBuilder.post(Entity.entity(empresaSeleccionada, MediaType.APPLICATION_XML));
        //this.empresaService.registrarEmpresa(empresaSeleccionada);
        this.empresas.add(empresaSeleccionada);
        this.empresaSeleccionada = null;
    }
    
    public void eliminarEmpresa(Empresa empresa){
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/empresas");
        String pathEliminar= "/" + empresa.getIdEmpresa();
        invocationBuilder = webTarget.path(pathEliminar).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.delete();
        this.empresas.remove(empresa);
        this.empresaSeleccionada = null;
    }
    
    public void reiniciarEmpresaSeleccionada(Empresa empresa){
        this.empresaSeleccionada = new Empresa();
    }

    public String getROL() {
        return ROL;
    }

    public void setROL(String ROL) {
        this.ROL = ROL;
    }

    
    
    
    
}
