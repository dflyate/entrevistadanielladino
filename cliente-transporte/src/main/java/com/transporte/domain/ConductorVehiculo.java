/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transporte.domain;

/**
 *
 * @author Usuario
 */
public class ConductorVehiculo {
    private Integer idConductorVehiculo;
    private Integer idConductor;
    private Integer idVehiculo;

    public ConductorVehiculo(Integer idConductorVehiculo) {
        this.idConductorVehiculo = idConductorVehiculo;
    }

    public ConductorVehiculo(Integer idConductor, Integer idVehiculo) {
        this.idConductor = idConductor;
        this.idVehiculo = idVehiculo;
    }

    public ConductorVehiculo() {
    }

    public Integer getIdConductorVehiculo() {
        return idConductorVehiculo;
    }

    public void setIdConductorVehiculo(Integer idConductorVehiculo) {
        this.idConductorVehiculo = idConductorVehiculo;
    }

    public Integer getIdConductor() {
        return idConductor;
    }

    public void setIdConductor(Integer idConductor) {
        this.idConductor = idConductor;
    }

    public Integer getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    @Override
    public String toString() {
        return "ConductorVehiculo{" + "idConductorVehiculo=" + idConductorVehiculo + ", idConductor=" + idConductor + ", idVehiculo=" + idVehiculo + '}';
    }
    
    
    
}
