package com.transporte.domain;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Vehiculo {
    
    private Integer idVehiculo;
    private String placa;
    private String motor;
    private String chasis;
    private String modelo;
    private String fechaMatricula;
    private int pasajerosSentados;
    private int pasajerosPie;
    private String pesoSeco;
    private String pesoBruto;
    private int puertas;
    private String marca;
    private String linea;
    private Integer idEmpresa;
    @Transient
    private boolean marcado;
    @Transient
    private boolean marcadoConductor;

    public Vehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public Vehiculo() {
    }
    
    public Integer getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFechaMatricula() {
        return fechaMatricula;
    }

    public void setFechaMatricula(String fechaMatricula) {
        this.fechaMatricula = fechaMatricula;
    }

    public int getPasajerosSentados() {
        return pasajerosSentados;
    }

    public void setPasajerosSentados(int pasajerosSentados) {
        this.pasajerosSentados = pasajerosSentados;
    }

    public int getPasajerosPie() {
        return pasajerosPie;
    }

    public void setPasajerosPie(int pasajerosPie) {
        this.pasajerosPie = pasajerosPie;
    }

    public String getPesoSeco() {
        return pesoSeco;
    }

    public void setPesoSeco(String pesoSeco) {
        this.pesoSeco = pesoSeco;
    }

    public String getPesoBruto() {
        return pesoBruto;
    }

    public void setPesoBruto(String pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "idVehiculo=" + idVehiculo + ", placa=" + placa + ", motor=" + motor + ", chasis=" + chasis + ", modelo=" + modelo + ", fechaMatricula=" + fechaMatricula + ", pasajerosSentados=" + pasajerosSentados + ", pasajerosPie=" + pasajerosPie + ", pesoSeco=" + pesoSeco + ", pesoBruto=" + pesoBruto + ", puertas=" + puertas + ", marca=" + marca + ", linea=" + linea + ", idEmpresa=" + idEmpresa + ", marcado=" + marcado + '}';
    }

    

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    
   

    public boolean isMarcado() {
        return marcado;
    }

    public void setMarcado(boolean marcado) {
        this.marcado = marcado;
    }

    public boolean isMarcadoConductor() {
        return marcadoConductor;
    }

    public void setMarcadoConductor(boolean marcadoConductor) {
        this.marcadoConductor = marcadoConductor;
    }
    
    
}
